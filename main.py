import argparse
import json
import pprint
import random
import string
import re
import subprocess

import boto3
from botocore.exceptions import ClientError


def randomStringDigits(stringLength=8):
    pattern = re.compile('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])')
    lettersAndDigits = string.ascii_letters + string.digits
    result = ''.join(random.choice(lettersAndDigits) for i in range(stringLength))
    while not pattern.match(result):
        result = ''.join(random.choice(lettersAndDigits) for i in range(stringLength))
    return result

pp = pprint.PrettyPrinter(indent=4)
CREATE_ACC_RESULTS_FILE = 'create_acc_results.json'
# string template for role that admins a child account
ORGANIZATION_ADMIN_ROLE_TEMPLATE = 'arn:aws:iam::{}:role/OrganizationAccountAccessRole'

####### USER SUPPLIED VALUES ##############
AWS_PROFILE='isengard'
MAX_ACCOUNTS = 200
PASSWORD_PREFIX='aws-pass'
# this will create emails like `awslabuser001@amazon.com
EMAIL_FORMAT='awslabuser{:03d}@amazon.com'

ROOT_ORGANIZATION_ID = 'r-hvus'
WORKSHOP_ORGANIZATION_ID = 'ou-hvus-ykg71pt2'

WORKMAIL_ORGANIZATION_ID='m-20b4937c9abc419ea30acb065c8aff82'
WORKMAIL_ORGANIZATION_NAME='awslabs1'


#############################################

def generate_account_list():
    acc_list = []
    for i in range(1, MAX_ACCOUNTS+1):
        acc_list.append({
            'email': EMAIL_FORMAT.format(i),
            'account_name': 'Workshop{:03d}'.format(i),
            'account_alias': '{}-wsu{:03d}'.format(WORKMAIL_ORGANIZATION_NAME, i),
            'workmail_password': '{}{:03d}'.format(PASSWORD_PREFIX, i),
            'workmail_name': 'wsu{:03d}'.format(i),
            'workmail_display_name': 'wsu{:03d}'.format(i),
            'workmail_email': 'wsu{:03d}@{}.awsapps.com'.format(i, WORKMAIL_ORGANIZATION_NAME)
        })
    return acc_list


def get_session(token=None, access_key_id=None, secret_access_key=None):
    if token or access_key_id or secret_access_key:
        # all three must be present
        if not token or not access_key_id or not secret_access_key:
            raise Error('token, access_key_id and secret_access_key parameters must all be present if any one is present')
        return boto3.Session(
            aws_access_key_id=access_key_id,
            aws_secret_access_key=secret_access_key,
            aws_session_token=token)
    else:
        return boto3.Session(profile_name=AWS_PROFILE)


def get_org_client(session):
    return session.client('organizations')

def get_workmail_client(session):
    return session.client('workmail', region_name='us-east-1')

def get_sts_client(session):
    return session.client('sts')

def get_iam_client(session):
    return session.client('iam')



def lazy_create_accounts(client, account_list):
    accounts_in_root = list_accounts(client, ROOT_ORGANIZATION_ID)
    move_accounts(
        client,
        [v for k,v in accounts_in_root.items()],
        [a['account_name'] for a in account_list])

    existing_accounts_dict = list_accounts(client, WORKSHOP_ORGANIZATION_ID)
    accounts_to_create_list = []
    for a in account_list:
        if a['account_name'] in existing_accounts_dict:
            print('{} exists'.format(a['account_name']), end = "\r")
        else:
            accounts_to_create_list.append(a)

    create_accounts(client, accounts_to_create_list)

    if accounts_to_create_list:
        print('Wait a while for accounts to be created.')
        exit(0)


def list_accounts(client, parent_id = WORKSHOP_ORGANIZATION_ID):
    results = {}
    next_token=None
    while True:
        # list_accounts_for_parents doesn't take kindly to None value for NextToken
        params = {
            'ParentId': parent_id,
        }
        if next_token:
            params['NextToken'] = next_token

        response = client.list_accounts_for_parent(**params)
        for a in response['Accounts']:
            results[a['Name']] = a
        next_token = response.get('NextToken', None)
        if not next_token:
            break

    return results


def create_accounts(client, account_list):
    results = {}
    for acc in account_list:
        print('creating {}...'.format(acc['account_name']))
        response = client.create_account(
            Email=acc['email'],
            AccountName=acc['account_name'],
            IamUserAccessToBilling='DENY'
        )
        payload  ={
            acc['email']: {
                'req_id': response['CreateAccountStatus']['Id']
            }
        }
        #print_response(payload)
        results.update(payload)

    with open(CREATE_ACC_RESULTS_FILE, 'w') as f:
        json.dump(results, f, indent=4)

def nuke_accounts(client, session, acc_list, count):
    list_with_account_ids = list_accounts(client)
    i = 0
    for key in sorted(list_with_account_ids.keys()):
        acct = list_with_account_ids[key]
        print("{} {}".format(key, acct))
        i += 1
        alias = "wsu{:03d}".format(i)
        nuke_account(session, acct['Id'], alias)
        if i >= count:
            break

def nuke_account(session, account_id, alias):
    target_role = ORGANIZATION_ADMIN_ROLE_TEMPLATE.format(account_id)
    sts_client = get_sts_client(session)
    res = sts_client.assume_role(RoleArn=target_role, RoleSessionName='{}_session'.format(alias))
    command = [
        "aws-nuke",
        "-c",
        "aws_nuke_config.yml",
        "--access-key-id",
        res['Credentials']['AccessKeyId'],
        "--secret-access-key",
        res['Credentials']['SecretAccessKey'],
        "--session-token",
        res['Credentials']['SessionToken'],
        "--force",
        "--force-sleep",
        "3",
        "--no-dry-run"
    ]
    print(alias)
    print_response(command)
    subprocess.call(command)

def set_account_aliases(session, acc_list):
    existing_accounts_dict = list_accounts(client, WORKSHOP_ORGANIZATION_ID)

    for a in acc_list:
        existing_acc = existing_accounts_dict.get(a['account_name'], None)
        if not existing_acc:
            raise('{} not found'.format(a['account_name']))

        print('{} - setting alias {}'.format(a['account_name'], a['account_alias']))
        lazy_set_account_alias(session, existing_acc['Id'], a['account_alias'])

def lazy_set_account_alias(session, account_id, alias):
    target_role = ORGANIZATION_ADMIN_ROLE_TEMPLATE.format(account_id)
    sts_client = get_sts_client(session)
    res = sts_client.assume_role(RoleArn=target_role, RoleSessionName='temp_{}'.format(alias))
    acc_session = get_session(
        token=res['Credentials']['SessionToken'],
        access_key_id=res['Credentials']['AccessKeyId'],
        secret_access_key=res['Credentials']['SecretAccessKey'])

    acc_iam_client = get_iam_client(acc_session)
    res = acc_iam_client.list_account_aliases()
    try:
        existing_alias = res['AccountAliases'][0]
        # if the alias is different, we need to delete it.
        if not alias == existing_alias:
            acc_iam_client.delete_account_alias(AccountAlias=existing_alias)
        else:
            # if they are the same we are done.
            return
    except IndexError:
        # means no aliases set
        pass

    # AWS Account aliases must be lowercase per docs
    res = acc_iam_client.create_account_alias(AccountAlias=alias.lower())




def move_accounts(client, account_list, acc_name_filter=[]):
    for acc in account_list:
        if acc_name_filter:
            if acc['Name'] not in acc_name_filter:
                continue

        print('Moving account {} from root to OU'.format(acc['Name']))
        res = client.move_account(
            AccountId=acc['Id'],
            SourceParentId=ROOT_ORGANIZATION_ID,
            DestinationParentId=WORKSHOP_ORGANIZATION_ID)


def account_create_status(client, account_list):
    results = {}
    with open(CREATE_ACC_RESULTS_FILE, 'r') as f:
        results = json.load(f)

    for email, r in results.items():
        req_id = r['req_id']
        res = client.describe_create_account_status(CreateAccountRequestId=req_id)
        status_dict = res['CreateAccountStatus']
        payload = {
            email: {
                'req_id': status_dict['Id'],
                'state': status_dict['State'],
                'detail': status_dict.get('FailureReason', None)
            }
        }
        print_response(payload)


def list_workmail_users(client, workmail_org_id):
    results = {}
    next_token=None
    while True:
        # list_accounts_for_parents doesn't take kindly to None value for NextToken
        params = {
            'OrganizationId': workmail_org_id,
            'MaxResults': 100,
        }
        if next_token:
            params['NextToken'] = next_token

        response = client.list_users(**params)
        for u in response['Users']:
            results[u['Name']] = u
        next_token = response.get('NextToken', None)
        if not next_token:
            break

    return results



def lazy_create_workmail_users(client, acc_list):
    existing_users_dict = list_workmail_users(client, WORKMAIL_ORGANIZATION_ID)
    accounts_to_create_list = []

    for a in acc_list:
        existing_user = existing_users_dict.get(a['workmail_name'], None)
        if existing_user:
            print('{} exists.'.format(existing_user['Name']), end = "\r")
            if existing_user['State'] == 'DISABLED':
                print('\tdisabled. Enabling...')
                client.register_to_work_mail(
                    OrganizationId=WORKMAIL_ORGANIZATION_ID,
                    EntityId=existing_user['Id'],
                    Email=a['workmail_email']
                    )
            print('\tresetting password...')
            client.reset_password(
                OrganizationId=WORKMAIL_ORGANIZATION_ID,
                UserId=existing_user['Id'],
                Password=a['workmail_password'])
        else:
            accounts_to_create_list.append(a)


    results = {}
    for acc in accounts_to_create_list:
        print('Creating {}'.format(acc['workmail_name']))
        # we don't check for duplicates here because that shouild already be done above
        response = client.create_user(
            OrganizationId=WORKMAIL_ORGANIZATION_ID,
            Name=acc['workmail_name'],
            DisplayName=acc['workmail_display_name'],
            Password=acc['workmail_password'],
        )
        print('\tdisabled. Enabling...')
        client.register_to_work_mail(
            OrganizationId=WORKMAIL_ORGANIZATION_ID,
            EntityId=response['UserId'],
            Email=acc['workmail_email']
        )

def reset_passwords(client, acc_list, workshop_accounts, count):
    existing_users_dict = list_workmail_users(client, WORKMAIL_ORGANIZATION_ID)
    i = 0
    for a in acc_list:
        existing_user = existing_users_dict.get(a['workmail_name'], None)
        suffix = existing_user['Name'][3:]
        index = 'Workshop' + suffix
        workshop_account_id = workshop_accounts[index]['Id']
        if existing_user:
            new_password = randomStringDigits()
            # new_password = 'aws-pass{}'.format(suffix)
            print('\nUsername: {} Password: {}'.format(existing_user['Name'], new_password))
            client.reset_password(
                OrganizationId=WORKMAIL_ORGANIZATION_ID,
                UserId=existing_user['Id'],
                Password=new_password)
            i += 1
            if i >= count:
                break


def z(client):
    res = client.list_roots()
    print_response(res)


def print_response(res):
    pp.pprint(res)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('action')
    parser.add_argument('--account_count', default=None, type=int)
    args = parser.parse_args()

    acc_list = generate_account_list()

    try:
        session = get_session()
        client = get_org_client(session)

        if args.action == 'z':
            z(client)
        elif args.action == 'lazy_create_accounts':
            print('Lazy Creating Accounts...')
            lazy_create_accounts(client, acc_list)
            print('Lazy Setting Accounts Aliases...')
            set_account_aliases(session, acc_list)
            client = get_workmail_client(session)
            print('Lazy Creating Workmail Users...')
            lazy_create_workmail_users(client, acc_list)
        elif args.action == 'list_acc':
            results = list_accounts(client)
            print_response(results)
            account_ids = [val['Id'] for key, val in sorted(results.items())]
            count = args.account_count or len(account_ids)
            print(",".join(account_ids[:count]))
        elif args.action == 'nuke_accounts':
            count = args.account_count or len(acc_list)
            nuke_accounts(client, session, acc_list, count)
        elif args.action == 'check_acc_create':
            account_create_status(client, acc_list)
        elif args.action == 'reset_passwords':
            accounts = list_accounts(client)
            client = get_workmail_client(session)
            count = args.account_count or len(accounts)
            reset_passwords(client, acc_list, accounts, count)

    except ClientError as err:
        print('----- ERROR ----')
        print(err)
        print('-----')
