# Iteratively try for
PY_BIN=`which python3.7`
 if [ -z "$PY_BIN" ]; then PY_BIN=`which python3.6`; fi
 if [ -z "$PY_BIN" ]; then PY_BIN=`which python3`; fi
 if [ -z "$PY_BIN" ]; then
    echo "Error: Python3 not installed"
    exit 1;
fi

echo "Using $PY_BIN";

# lazy create virtual env
if test ! -d .venv; then $PY_BIN -m venv .venv; fi
# activate virtual env
source .venv/bin/activate
# install reqs
pip install -q -r requirements.txt

echo "Success";
exit 0;